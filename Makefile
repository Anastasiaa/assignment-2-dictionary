NASM=nasm
NASM_OPTS=-f elf64
LD=ld -o
RM=rm -rf
PYTHON=/usr/bin/python3.6

.PHONY: all test clean


all: dictionary test

test:
	${PYTHON} tests.py

dictionary: main.o dict.o lib.o
	ld -o $@ $^

clean:
	${RM} *.o *.lst dictionary report.xml test_* __pycache__


%.o: %.asm
	$(NASM) $(NASM_OPTS) $< -o $@

lib.o: lib.asm lib.inc

dict.o: dict.asm dict.inc

main.o: main.asm lib.inc dict.inc words.inc colon.inc
