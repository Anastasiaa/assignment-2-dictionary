global _start

%include "words.inc"
%include "lib.inc"
%include "dict.inc"

%define BUFFER_SIZE 256
%define POINTER_SIZE 8
%define EXIT_CODE_ERROR 1


section .rodata
    key_not_found: db "Key is not found.", 0
    key_too_long: db "Key is too long.", 0

section .bss
	buf: resb (BUFFER_SIZE + 1)


section .text


_start:
    mov rdi, buf
    mov rsi, BUFFER_SIZE
    call read_string
    test rax, rax
    je .not_length
    push rdx
    mov rdi, rax           
    mov rsi, last
    call find_word
    test rax, rax
    je .not_found
    pop rdx 
    mov rdi, rax
    add rdi, POINTER_SIZE
    add rdi, rdx
    inc rdi
    call print_string
    call print_newline
    xor rdi, rdi
    jmp exit
.not_found:
    mov rdi, key_not_found
    jmp .end
.not_length:
    mov rdi, key_too_long
.end:
    call print_error
    call print_newline
    mov rdi, EXIT_CODE_ERROR
    jmp exit

