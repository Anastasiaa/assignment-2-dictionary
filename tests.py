import subprocess

sin = [
    'o n e',
    'two',
    'tree',
    'i' * 300,
    ''
        ]

sout = [
    'one explanation',
    'two explanation',
    '',
    '',
    ''
        ]

serr = [
    '',
    '',
    'Key is not found.',
    'Key is too long.',
    'Key is not found.'
        ]

passed_tests = 0

for i in range(len(sin)):
    test_data = [sin[i], sout[i], serr[i]]

    pr = subprocess.Popen(['./dictionary'], stdin = subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    stdout, stderr = pr.communicate(input=test_data[0].encode())
    stdout, stderr = stdout.decode().strip(), stderr.decode().strip()

    print(f'Test {i + 1}:')

    if stdout == test_data[1] and stderr == test_data[2]:
        print('Test passed: ')
        print(f'\tSTDIN:  {test_data[0]}')
        print(f'\tSTDOUT: {test_data[1]}')
        print(f'\tSTDERR: {test_data[2]}')
        passed_tests += 1
    else:
        if stdout != test_data[1]:
            print(f'Incorrect DATA output:"{stdout}" Expected:"{test_data[1]}" for input"{test_data[0]}"\n')
        if stderr != test_data[2]:
            print(f'Incorrect ERROR output:"{stderr}" Expected:"{test_data[2]}" for input"{test_data[0]}"\n')

print('\nRESULTS')
print(f'\tPASSED: {passed_tests} | {len(sin)}')
print(f'\tFAILED: {len(sin) - passed_tests} | {len(sin)}')
