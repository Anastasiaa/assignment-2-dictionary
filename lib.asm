global exit
global string_length
global print_string
global print_err
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
global read_string



%define SYS_READ 0
%define SYS_WRITE 1
%define STDERR 2
%define STDOUT 1
%define STDIN 0
%define SYS_EXIT 60
%define STRING_END 0
%define WHITESPACE ' '
%define TABULATION `\t`
%define LINEBREAK `\n`

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; аргументы: rdi -- указатель на нуль-терминированную строку
; возвращает: rax -- длина строки
string_length:
	xor rax, rax
.loop:
	cmp byte [rdi + rax], 0
	je .end
	inc rax
	jmp .loop
.end:
	ret

print_string:
    push rdi
    call string_length
    pop rsi
    mov  rdx, rax
    mov  rax, SYS_WRITE
    mov  rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, LINEBREAK

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
	mov rsi, rsp
    mov rdx, 1
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    syscall
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	enter 0, 0
	mov rax, rdi
	push STRING_END       ; сохранение конца строки в стек
	mov rdi, 10
.loop:   
	xor rdx, rdx
	div rdi
	add dl, '0'           ; превращаем символ в цифру
	dec rsp
	mov byte[rsp], dl     ; сохранение цифры в стеке
	test rax, rax
	je .end
	jmp .loop
 .end:
	mov rdi, rsp          
	call print_string
	leave
	ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi
	jge .positive         ; число положительное, просто выводим
	push rdi
	mov rdi, '-'
	call print_char  	  ; вывести знак - и вывести после само число
	pop rdi
	neg rdi
.positive: 
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
.loop:
	mov al, byte[rsi]
	cmp byte[rdi], al
	jne .not_equals
	cmp byte[rdi], 0
	je .equals
	inc rsi
	inc rdi
	jmp .loop
.not_equals:
	xor rax, rax
	ret
.equals:
	mov rax, 1
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	push 1
	mov rdi, STDIN
	mov rsi, rsp
	mov rdx, 1
	xor rax, rax
	syscall
	test rax, rax
	jg .end
	pop rax
	xor rax, rax
	ret
 .end:   
	pop rax
	ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push rsi
    push rdi
    mov r12, rdi
    mov r13, rsi
.skip:					; цикл пропуска пробельных символов до слова
    call read_char
    test rax, rax
    je .eof
    cmp al, WHITESPACE
    je .skip
    cmp al, TABULATION
    je .skip
    cmp al, LINEBREAK
    je .skip
.cycle:					; читаем слово
    dec r13
    test r13, r13
    je .of
    mov [r12], al
    inc r12
    call read_char
    test rax, rax
    je .eof
    cmp al, WHITESPACE
    je .endWord
    cmp al, TABULATION
    je .endWord
    cmp al, LINEBREAK
    je .endWord
    jmp .cycle
.endWord:
    xor al,al
.eof:
    mov [r12], al
    pop rax
    pop rdx
    sub rdx, r13
    jmp .fin
.of:
    pop rax
    xor rax, rax
    pop rdx
.fin:
    pop r13
    pop r12
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax         ; число
	xor rcx, rcx         ; длина
	mov r8, 10           ; коэффициент
 .loop:   
	cmp byte[rdi], STRING_END
	je .end
	cmp byte[rdi], '0'   ; если больше '9' или меньше '0' прекращаем проверку
	jl .end
	cmp byte[rdi], '9'
	jg .end
	inc rcx              ; счетчик + 1
	mul r8
	mov dl, byte[rdi]    ; читаем цифру
	sub dl, '0'          ; превращаем символ в цифру
	add al, dl           ; добавляем цифру к числу
	inc rdi              ; увеличиваем адрес строки
	jmp .loop        
 .end:  
	mov rdx, rcx         ; возвращаем длину числа
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	cmp byte[rdi], '-'
	je .minus          ; число отрицательное
	jmp parse_uint
 .minus:
	inc  rdi 
	call parse_uint
	test rdx, rdx     ; если не число - выходим
	je .end
	neg  rax        
	inc  rdx        ; учеличиваем длину числа на 1 из-за знака '-'
 .end:  
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax            ; счетчик
.loop:    
	cmp rax, rdx            ; если счетчик равен буферу - длина = 0
	je  .fail
	cmp byte[rdi], STRING_END    ; если конец - success
	je  .success
	inc rax                 ; счетчик + 1
	mov cl, byte[rdi]       ; чтение символа
	mov byte[rsi], cl       ; запись символа
	inc rsi                 ; увеличиваем адрес на 1
	inc rdi
	jmp .loop
.fail:  
	xor rax, rax 
	jmp .end
.success:  
	mov byte[rsi], STRING_END
.end:   
	ret

print_error:
    push rdi
    call string_length
    mov  rdx, rax
    pop  rsi
    mov  rax, SYS_WRITE
    mov  rdi, STDERR
    syscall
    jmp print_newline

read_string:
    push r12
    push r13
    push rsi
    push rdi
    mov r12, rdi
    mov r13, rsi
.skip:					; цикл пропуска пробельных символов до слова
    call read_char
    test rax, rax
    je .eof
    cmp al, WHITESPACE
    je .skip
    cmp al, TABULATION
    je .skip
    cmp al, LINEBREAK
    je .skip
.cycle:					; читаем слово
    dec r13
    test r13, r13
    je .of
    mov [r12], al
    inc r12
    call read_char
    test rax, rax
    je .eof
    cmp al, LINEBREAK
    je .endWord
    jmp .cycle
.endWord:
    xor al,al
.eof:
    mov [r12], al
    pop rax
    pop rdx
    sub rdx, r13
    jmp .fin
.of:
    pop rax
    xor rax, rax
    pop rdx
.fin:
    pop r13
    pop r12
    ret